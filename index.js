let [hour,minute,second] = new Date().toTimeString().split(' ')[0].split(':')

function getTime() {
    if (second > -1) {
        document.getElementById("second").innerHTML = second
        document.getElementById("minute").innerHTML = minute
        document.getElementById("hour").innerHTML = hour
        second--
        second = second.toString().padStart(2, 0)
    } else if (second == -1 && minute != 0) {
        minute--
        minute = minute.toString().padStart(2, 0)
        second = 59
    } else {
        hour--
        if (hour < 0) hour = 23
        hour = hour.toString().padStart(2, 0)
        minute = 60
    }
}
// setInterval(getTime, 1000);
